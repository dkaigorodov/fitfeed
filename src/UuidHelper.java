import java.util.Random;

public abstract class UuidHelper {

    private UuidHelper() {
    }

    private static String[] ALPHABET = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
    private static int DEFAULT_UUID_LENGTH = 16;
    private static Random random = new Random();

    public static String generateRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; ++i) {
            sb.insert(i, ALPHABET[random.nextInt(16)]);
        }
        return sb.toString();
    }

    public static String generateRandomString() {
        return generateRandomString(DEFAULT_UUID_LENGTH);
    }
    
}
