import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dkaigorodov on 12/5/15.
 */
public class GetToken extends AbstractJsonServlet {
    static public Map<String, UserSlot> pin2user = new HashMap<>();


    @Override
    public JsonObject service(JsonObject params) throws Exception {
        JsonObject result = new JsonObject();
        if (params.has("pin")) {
            String pin = params.getAsJsonPrimitive("pin").getAsString();

            User user = pin2user.remove(pin).getUser();
            if (user != null) {
                result.addProperty("token", user.getToken());
                result.addProperty("userName", user.getName());
            }

        } else {
            String value = generatePin();
            result.addProperty("pin", value);
            pin2user.put(value, new UserSlot());

        }

        return result;
    }

    private String generatePin() {
        int pin = (int) (Math.random() * 9999);
        if (pin < 1000) {
            pin += 1000;
        }
        String result = pin + "";
        if (pin2user.containsKey(result)) {
            return generatePin();
        }
        return result;
    }
}
