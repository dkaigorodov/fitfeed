/**
 * Created by dkaigorodov on 12/5/15.
 */
public class User {

    public final static User me = new User("Дмитрий Кайгородов", UuidHelper.generateRandomString());

    private final String token;
    private final String name;

    public User(String name, String token) {
        this.name = name;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public String getName() {
        return name;
    }
}
