import com.google.gson.JsonObject;

/**
 * Created by dkaigorodov on 12/5/15.
 */
public class InputPin extends AbstractJsonServlet {



    @Override
    public JsonObject service(JsonObject params) throws Exception {
        JsonObject result = new JsonObject();
        String pin = params.get("pin").getAsString();

        UserSlot userSlot = GetToken.pin2user.get(pin);
        if (userSlot != null) {
            result.addProperty("success", true);
            userSlot.setUser(User.me);

        } else {
            result.addProperty("success", false);
            result.addProperty("error", "Unknown PIN");
        }


        return result;
    }
}
