/**
 * Created by dkaigorodov on 12/6/15.
 */
public class UserSlot {
    private  User user = null;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
