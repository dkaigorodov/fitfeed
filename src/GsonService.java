
import com.google.gson.*;

import java.io.Reader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class GsonService {

    private static final Gson gson = new Gson();
    private static final JsonParser jsonParser = new JsonParser();

    public static String toJson(Object object) {
        return gson.toJson(object);
    }

    public static <T> T fromJson(String source, Class<T> clazz) {
        return gson.fromJson(source, clazz);
    }

    public static <T> T fromJson(Reader reader, Class<T> clazz) {
        return gson.fromJson(reader, clazz);
    }

    public static <T> List<T> toList(JsonArray array, Class<T> clazz) {
        List<T> list = new ArrayList<>(array.size());
        for (JsonElement element : array) {
            list.add(gson.fromJson(element, clazz));
        }
        return list;
    }

    public static JsonObject toJsonObject(String src) {
        return (JsonObject) jsonParser.parse(src);
    }

    public static JsonObject toJsonObject(Reader reader) {
        return (JsonObject) jsonParser.parse(reader);
    }

    public static <T> JsonArray toDtoJsonArray(List<T> list, Class<T> objectClass, Class<?> dtoClass) {
        JsonArray result = new JsonArray();

        Constructor<?> constructor;
        try {
            constructor = dtoClass.getConstructor(objectClass);

            for (T obj : list) {
                Object dtoObj = constructor.newInstance(obj);
                result.add(gson.toJsonTree(dtoObj));
            }

            return result;
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
            throw new RuntimeException("Reflection error ", e);
        }
    }

    public static <T, R> List<R> toDtoArray(List<T> list, Class<T> objectClass, Class<R> dtoClass) {
        if (list == null) {
            return null;
        }
        List<R> result = new ArrayList<>(list.size());

        Constructor<R> constructor;
        try {
            constructor = dtoClass.getConstructor(objectClass);

            for (T obj : list) {
                if (obj != null) {
                    R dtoObj = constructor.newInstance(obj);
                    result.add(dtoObj);
                }
            }

            return (List<R>) result;
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
            throw new RuntimeException("Reflection error ", e);
        }
    }

    public static <T> JsonArray toJsonArray(List<T> list) {
        JsonArray result = new JsonArray();

        for (T obj : list) {
            result.add(gson.toJsonTree(obj));
        }

        return result;
    }

    public static List<Long> toLongArray(JsonArray jsonArray) {
        ArrayList<Long> result = new ArrayList<>(jsonArray.size());
        for (JsonElement jsonElement : jsonArray) {
            result.add(jsonElement.getAsLong());
        }
        return result;
    }

    public static String[] toStringArray(JsonArray jsonArray) {
        String[] result = new String[jsonArray.size()];
        for (int i = 0; i < jsonArray.size(); ++i) {
            JsonElement jsonElement = jsonArray.get(i);
            result[i] = jsonElement.getAsString();
        }
        return result;
    }
}
