
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: dkaigorodov
 * Date: 8/28/14
 * Time: 9:11 AM
 */
public abstract class AbstractJsonServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Methods", "POST, OPTIONS");

        if (req.getMethod().equals("POST")) {

//            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
//            Transaction txn = datastore.beginTransaction();

            JsonObject result = new JsonObject();
            try {
                try {
                    log("Running: " + req.getRequestURI());
                    JsonObject params = GsonService.toJsonObject(req.getReader());
                    result = service(params);
                } catch (IllegalArgumentException | AssertionError e) {
                    result.addProperty("errorMsg", e.getMessage());
                    log("IllegalArgumentException while request processing", e);
                }

                sendResponse(resp, result);

            } catch (Exception e) {
                throw new ServletException(e);
            } finally {
            }

        }
    }

    /**
     * Called before sending response. Override to get access to HttpServletResponse object from subclass.
     */
    protected void processResponse(HttpServletResponse resp) {
    }

    public abstract JsonObject service(JsonObject params) throws Exception;

    private void sendResponse(HttpServletResponse resp, JsonObject jsonResp) throws IOException {
        processResponse(resp);

        if (jsonResp != null) {
            String responseString = jsonResp.toString();
            resp.setContentType("application/json");
            resp.setContentLength(responseString.length());
            resp.getWriter().write(responseString);
        }
    }

}
